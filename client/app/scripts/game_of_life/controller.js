'use strict';

angular.module('Game_of_life')
.controller('game_of_life', function ($scope) {
  $scope.grid_of_game = [[ 1, 0]];
  
  $scope.create_grid = function(cols, rows) {
    $scope.grid_of_game = [];

    for(var col=0; col < cols; col++) {
      $scope.grid_of_game.push([]);
      
      for(var row=0; row < rows; row++) {
        $scope.grid_of_game[col].push(0);  
      }
    }
  };
  
  $scope.on_cells = function(switch_on_cells) {
    switch_on_cells.forEach(function(alive_cell){
      $scope.grid_of_game[alive_cell.col][alive_cell.row] = 1;
    });
  };
  
  $scope.next_generation = function() {
    var grid_clone = [];
    
    $scope.grid_of_game.forEach(function(rows, row) {
      grid_clone.push(rows.slice(0, $scope.grid_of_game[row].length));
    });
    
    grid_clone.forEach(function(rows, row){
      rows.forEach(function(cell, col) {
        var neigthbors = get_alive_neigthbors(grid_clone, row, col);
        
        if(neigthbors >= 3) {
          $scope.grid_of_game[row][col] = 1;
        }
        
        if(neigthbors < 2) {
          $scope.grid_of_game[row][col] = 0;
        }
        
      });
    });
  };
  
  function get_alive_neigthbors(grid_clone, row, col) {
    var alive_cells = 0;

    if(row - 1 >= 0) {
      alive_cells += get_alive_cell(grid_clone[row - 1].slice(col - 1, col + 2));
    }

    if(row + 1 < grid_clone.length) {
      alive_cells += get_alive_cell(grid_clone[row + 1].slice(col - 1, col + 2));
    }
    
    alive_cells += get_alive_cell([grid_clone[row][col - 1]]);
    alive_cells += get_alive_cell([grid_clone[row][col + 1]]);
    
    return alive_cells;
  }
  
  function get_alive_cell(cells) {
    var count_cells = 0;
    
    cells.forEach(function(cell) {
      if(cell) { count_cells++; }
    });
    
    return count_cells;
  }
})
.config(function ($routeProvider) {
  $routeProvider
  .when('/game_of_life', {
    templateUrl: 'scripts/game_of_life/views/game_of_life.html',
    controller: 'game_of_life'
  });
});
