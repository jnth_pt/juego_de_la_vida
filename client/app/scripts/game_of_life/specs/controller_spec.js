'use strict';

describe('Controller: game_of_life', function () {

  beforeEach(module('Game_of_life'));

  var controller;
  var scope;

  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    controller = $controller('game_of_life', { $scope: scope });
  }));

  describe('On instance', function(){
    it('should set "grid_of_game" variable in scope', function() {
      expect(scope.grid_of_game).toEqual([]);
    });
  });
  
  describe('Inicialize grid', function(){

    beforeEach(function(){
      scope.create_grid(3,4);
    });

    it('should get grid to game of life', function() {
      expect(scope.grid_of_game).toEqual([[0,0,0,0],[0,0,0,0],[0,0,0,0]]);
    });
    
    it('should get grid to game of life', function() {
      var switch_on_cells = [
        { col: 1, row: 3 },
        { col: 2, row: 1 },
      ];
      
      scope.on_cells(switch_on_cells);

      expect(scope.grid_of_game).toEqual([[0,0,0,0],[0,0,0,1],[0,1,0,0]]);
    });
  });
  
  describe('Next generation grid', function(){

    it('should switch on cells', function() {
       scope.grid_of_game = [
        [0,0,0,0],
        [0,1,1,0],
        [0,1,0,0],
        [0,0,0,0]
      ];

      scope.next_generation();
      
      expect(scope.grid_of_game).toEqual([
        [0,0,0,0],
        [0,1,1,0],
        [0,1,1,0],
        [0,0,0,0]
      ]);
      
      scope.next_generation();
      
      expect(scope.grid_of_game).toEqual([
        [0,0,0,0],
        [0,1,1,0],
        [0,1,1,0],
        [0,0,0,0]
      ]);
    });
    
    it('should switch on cells', function() {
      
      scope.grid_of_game = [
        [0,0,0,0,0],
        [0,0,1,0,0],
        [0,0,1,0,0],
        [0,0,1,0,0],
        [0,0,0,0,0]
      ];
      
      scope.next_generation();
      
      expect(scope.grid_of_game).toEqual([
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,1,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
      ]);
    });
    
    it('should switch on cells', function() {
      
      scope.grid_of_game = [
        [0,0,0,0],
        [0,0,1,0],
        [0,1,0,0],
        [0,1,0,0],
        [0,0,0,0]
      ];
      
      scope.next_generation();
      
      expect(scope.grid_of_game).toEqual([
        [0,0,0,0],
        [0,0,0,0],
        [0,1,1,0],
        [0,0,0,0],
        [0,0,0,0]
      ]);
    });
    
    it('should switch on cells', function() {
      
      scope.grid_of_game = [
        [0,0,0,0],
        [0,1,0,1],
        [0,0,1,0]
      ];
      
      scope.next_generation();
      
      expect(scope.grid_of_game).toEqual([
        [0,0,0,0],
        [0,0,1,0],
        [0,0,1,0]
      ]);
    });
  });

  describe('when going to /game_of_life', function() {

    var route, location, rootScope, httpBackend;

    beforeEach(inject(function($route, $location, $rootScope, $httpBackend){
      route = $route;
      location = $location;
      rootScope = $rootScope;
      httpBackend = $httpBackend;

      httpBackend.when('GET', 'scripts/game_of_life/views/game_of_life.html').respond('<div></div>');
    }));

    afterEach(function() {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should use game_of_life.html and controller', function() {
      expect(route.current).toBeUndefined();

      location.path('/game_of_life');

      httpBackend.flush();

      expect(route.current.templateUrl).toBe('scripts/game_of_life/views/game_of_life.html');
      expect(route.current.controller).toBe('game_of_life');
    });
  });

});
